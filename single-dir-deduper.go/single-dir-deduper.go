package main

import (
	"crypto/md5"
	"errors"
	"flag"
	"fmt"
	"io"
	"os"
	"path/filepath"
)

type file struct {
	path          string
	name          string
	size          int64
	checksum      string
	shortChecksum string
	duplicated    bool
}

type config struct {
	dir        string
	outputFile string
	delete     bool
	verbose    bool
}

var verbose bool

func main() {
	config, err := getConfig()

	if err != nil {
		exitWithError(err, 1, true)
	}

	files, err := getFilesData(config.dir)

	filesToDelete, err := findAllDuplicates(files, config)

	for _, v := range filesToDelete {
		if config.delete {
			if verbose {
				fmt.Println("Deleting ", v.path)
				err := os.Remove(v.path)
				if err != nil {
					fmt.Println("Failed to remove file ", v.path, " - ", err.Error())
				}
			}
		} else {
			fmt.Println(v.checksum, "\t", v.path)
		}
	}
}

// getConfig parses command line arguments and returns a config struct with the right settings
// or generates an error containing all of the error messages
func getConfig() (config, error) {
	dir := flag.String("s", "", "Source directory.")
	outputFile := flag.String("o", "", "File to output the list of files to delete.")
	verbose := flag.Bool("v", false, "Verbose reporting.")
	delete := flag.Bool("d", false, "Delete duplicated files")

	flag.Parse()
	var conf config

	conf.dir = *dir
	conf.outputFile = *outputFile
	conf.verbose = *verbose
	conf.delete = *delete
	errorMessages := ""
	
	if conf.dir == "" {
		errorMessages = "A directory is required. If you want to use the current directory, use '.'.\n"
	}

	if errorMessages != "" {
		fmt.Println(errorMessages)
		return conf, errors.New(errorMessages)
	}
	return conf, nil
}

// getFilesData recursively walks around a directory and records all the files it finds
// optionally, it also calculates the checksum for the file
// returns an array of file structs and the error state
func getFilesData(dir string) ([]file, error) {
	files := make([]file, 0)

	fileData := func(path string, f os.FileInfo, err error) error {
		switch mode := f.Mode(); {
		case mode.IsDir():
			// do nothing
		case mode.IsRegular():
			var checksum string
			checksum, err = calculateFileChecksum(path)
			if err != nil {
				return errors.New("Failed to calculate the checksum for " + path)
			}
			files = append(files, file{name: f.Name(), path: path, size: f.Size(), checksum: checksum, duplicated: false})
		}
		return nil
	}

	f, err := os.Stat(dir)
	if err != nil {
		return nil, errors.New("Unable to find directory or file " + dir)
	}
	if !f.Mode().IsDir() {
		return nil, errors.New(dir + " is not a directory")
	}

	err = filepath.Walk(dir, fileData)

	return files, nil
}

// calculateFileChecksum calculates the checksum of a given file
// returns an string with the checksum and an error
// warning - reads the whole file in memory so don't use with huge files
func calculateFileChecksum(filePath string) (string, error) {
	var result []byte
	file, err := os.Open(filePath)
	if err != nil {
		return "", err
	}
	defer file.Close()

	// TODO - stream the file
	hash := md5.New()
	if _, err := io.Copy(hash, file); err != nil {
		return "", err
	}

	return fmt.Sprintf("%x", hash.Sum(result)), nil
}

// findAllDuplicates finds all the files that duplicated in the original slice
// returns a slice of file structs
func findAllDuplicates(sourceFiles []file, conf config) ([]file, error) {
	duplicates := make([]file, 0)
	for i, v := range sourceFiles {
		duplicates = append(duplicates, findDuplicates(v, sourceFiles, i+1, conf)...)
	}
	return duplicates, nil
}

// findDuplicates finds all duplicates of a given file, according to the config settings
// returns a slice of file structs
func findDuplicates(f file, comparison []file, index int, conf config) []file {
	duplicates := make([]file, 0)

	for i := index; i < len(comparison); i++ {
		// If the file is marked as a duplicate, skip
		if comparison[i].duplicated {
			continue
		}
		// Files with different size cannot have the same contents
		//  do not match if, somehow, we have got the same path in both sides!
		if f.size == comparison[i].size && f.path != comparison[i].path {
			// We're matching on md5
			if f.checksum == comparison[i].checksum {
				duplicates = append(duplicates, comparison[i])
				comparison[i].duplicated = true
			}
		}
	}
	return duplicates
}

// printError prints the specified error message, the usage and then exits with the specified error code
func exitWithError(err error, errorCode int, showFlags bool) {
	fmt.Println(err)
	if showFlags {
		fmt.Println("Options: ")
		flag.PrintDefaults()
	}
	os.Exit(errorCode)
}
