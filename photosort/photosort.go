package main

import (
	"errors"
	"flag"
	"fmt"
	"log"
	"os"
	"path"
	"path/filepath"
	"time"

	"github.com/rwcarlsen/goexif/exif"
)

// Photosort will sort your images, videos and any other file it can understand into date-based folders in ISO-8601 format via EXIF information.

// config contains configuration information
type config struct {
	source     string
	target     string
	merge      bool
	overwrite  bool
	verbose    bool
	dieOnError bool
}

// imageFile contains information for files
type imageFile struct {
	name      string
	path      string
	date      time.Time
	targetDir string
}

// getConfig extracts and validates command line options and builds a configuration struct
// returns a config struct
func getConfig() (config, error) {
	source := flag.String("s", "", "Source directory.")
	target := flag.String("t", "", "Target directory.")

	merge := flag.Bool("m", false, "Merge existing directories.")
	verbose := flag.Bool("v", false, "Verbose reporting.")
	overwrite := flag.Bool("o", false, "Overwrite existing files.")
	dieOnError := flag.Bool("d", false, "Exit if we find an error")

	flag.Parse()
	var conf config
	conf.source = *source
	conf.target = *target
	conf.merge = *merge
	conf.verbose = *verbose
	conf.overwrite = *overwrite
	conf.dieOnError = *dieOnError

	errorMessages := ""
	if conf.source == "" {
		errorMessages = "Source directory is required.\n"
	}

	if conf.target == "" {
		errorMessages += "Target directory is required.\n"
	}

	if errorMessages != "" {
		fmt.Println(errorMessages)
		return conf, errors.New(errorMessages)
	}
	return conf, nil
}

// verifyPaths checks that the source and destination dirs are not the same
func verifyPaths(source string, destination string) bool {
	return source != destination
}

// getFilesData recursively gets the data of all the files inside a directory
func getFilesData(dir string, conf config) ([]imageFile, error) {
	files := make([]imageFile, 0)

	fileData := func(path string, f os.FileInfo, err error) error {
		switch mode := f.Mode(); {
		case mode.IsDir():
			// do nothing
		case mode.IsRegular():
			date, err := getCreationDate(path, conf.verbose)
			if err != nil {
				errorMessage := "Unable to process file " + path + ": " err.Error()
				if conf.verbose {
					fmt.Println(errorMessage)
				}
				if conf.dieOnError {
					return errors.New(errorMessage)
				}
			} else {
				files = append(files, imageFile{name: f.Name(), path: path, date: date})
			}
		}
		return nil
	}

	f, err := os.Stat(dir)
	if err != nil {
		return nil, errors.New("Unable to find directory or file " + dir)
	}
	if !f.Mode().IsDir() {
		return nil, errors.New(dir + " is not a directory")
	}

	err = filepath.Walk(dir, fileData)

	if err != nil {
		return files, err
	}
	return files, nil
}

// getDates extracts the dates of a slice of imageFiles
func getDates(files []imageFile) []string {
	dates := make([]string, 0)

	for _, f := range files {
		d := f.date.Format("2006-01-02")
		if !inList(dates, d) {
			dates = append(dates, d)
		}
	}

	return dates
}

// inList checks if a slice of strings contains a given string
func inList(list []string, value string) bool {
	for _, v := range list {
		if v == value {
			return true
		}
	}
	return false
}

// createDirs creates directories in a given directory from a list of names
func createDirs(dirs []string, parentDir string, merge bool) error {
	for _, d := range dirs {
		dest := path.Join(parentDir, d)
		if stat, err := os.Stat(dest); err == nil && stat.IsDir() {
			// path is a directory and we merge, no need to create it
			break
		}

		err := os.Mkdir(dest, 0755)
		if err != nil {
			return errors.New("Failed to create dir " + dest + ". Error: " + err.Error())
		}
	}
	return nil
}

// matchDir returns a given string if it's contained in a slice of strings
func matchDir(fileDate string, dirs []string) (string, error) {
	for _, d := range dirs {
		if fileDate == d {
			return d, nil
		}
	}
	return "", errors.New("No matching destination directory for " + fileDate)
}

// moveFiles moves files to a target directory
func moveFiles(files []imageFile, dirNames []string, parentDir string, conf config) {
	for _, f := range files {
		d := f.date.Format("2006-01-02")

		targetDir, err := matchDir(d, dirNames)
		if err != nil {
			if conf.dieOnError {
				exitWithError(err, 1, false)
			}
			if conf.verbose {
				fmt.Println(err)
			}
		}
		destFile := path.Join(parentDir, targetDir, f.name)
		if conf.verbose {
			fmt.Println("Moving " + f.path + " to " + destFile)
		}
		err = os.Rename(f.path, destFile)
		if err != nil {
			if conf.dieOnError {
				log.Fatal(err)
			}
			if conf.verbose {
				fmt.Println(err)
			}
		}
	}
}

// getCreationDate opens a file and extracs creation date from its exif information
func getCreationDate(filename string, verbose bool) (time.Time, error) {
	var d time.Time

	if verbose {
		fmt.Println("Processing " + filename)
	}

	f, err := os.Open(filename)
	if err != nil {
		return d, err
	}

	x, err := exif.Decode(f)
	if err != nil {
		return d, err
	}

	d, err = x.DateTime()
	if err != nil {
		return d, err
	}

	return d, nil
}

// printError prints the specified error message, the usage and then exits with the specified error code
func exitWithError(err error, errorCode int, showFlags bool) {
	fmt.Println(err)
	if showFlags {
		fmt.Println("Options: ")
		flag.PrintDefaults()
		// printUsage()
	}
	os.Exit(errorCode)
}

func main() {
	conf, err := getConfig()
	if err != nil {
		exitWithError(err, 1, true)
	}

	if !verifyPaths(conf.source, conf.target) {
		exitWithError(errors.New("Source and destination are the same directory"), 1, false)
	}

	fileData, err := getFilesData(conf.source, conf)
	if err != nil {
		exitWithError(err, 1, false)
	}

	destDirs := getDates(fileData)

	err = createDirs(destDirs, conf.target, conf.merge)
	if err != nil {
		exitWithError(err, 1, false)
	}
	moveFiles(fileData, destDirs, conf.target, conf)
}
