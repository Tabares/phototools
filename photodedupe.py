#!/usr/bin/env python3

import typing
import hashlib


class Config:
    def __init__(self):
        source: str = ""
        comparison = []
        output_file: str = ""
        delete: bool = False
        match_filename: bool = False
        match_md5: bool = False
        verbose: bool = False
        separator: str = ","


class File:
    def __init__(self):
        path: str = ""
        name: str = ""
        size: int = 0
        checksum: str = ""
        short_checksum: str = ""


def parse_opts():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-s", "--source", dest="source", help="Directory to get pictures from."
    )
    parser.add_argument(
        "-c",
        "--comparison",
        dest="comparison",
        help="Directories to check pictures on, separated by --separator",
    )
    parser.add_argument(
        "-o", "--output", dest="output_file", help="File to output checks to"
    )
    parser.add_argument(
        "-d",
        "--delete",
        dest="delete",
        action="store_true",
        default=False,
        help="Delete duplicates",
    )
    parser.add_argument(
        "-f",
        "--match-filename",
        dest="match_filename",
        action="store_true",
        default=False,
        help="Match on filename",
    )
    parser.add_argument(
        "-m",
        "--md5",
        dest="match_md5",
        action="store_true",
        default=False,
        help="Match on MD5",
    )
    parser.add_argument(
        "-p",
        "--separator",
        dest="separator",
        default=",",
        help="Separator for --comparison",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        dest="verbose",
        action="store_true",
        default=False,
        help="Enable verbose mode.",
    )

    return parser.parse_args()


def get_config(options):
    config = Config()
    errors = []

    if not options.source:
        errors.append("Source directory is required")
    else:
        config.source = options.source

    if not options.comparison:
        errors.append("Comparison directories are required")
    else:
        config.comparison = options.comparison.split(options.separator)

    if options.source in options.comparison and errors == []:
        errors.append("Source and destination can not be the same")

    if errors:
        exitWithStatus("\n".join(errors), 1)

    config.match_filename = options.match_filename
    config.md5 = options.md5
    config.separator = options.separator
    config.verbose = options.verbose

    return config


def calculate_file_checksum(file_path: str) -> str:
    file = open(file_path, "r")
    checksum = hashlib.md5(file.read())
    file.close()
    return checksum


options = parse_opts()
config = get_config(options)
# if verbose {
# fmt.Println("Comparing", config.source, "with", strings.Join(config.comparison, ", "))
# }
verify_folders(config.source, config.comparison)
# err = verifyFolders(config.source, config.comparison)
# if err != nil {
# exitWithError(err, 4, false)
# }

try:
    source_files = get_files_data(config.source, config.match_md5)
except:
    exit_with_error(
        errors.New(
            "Error getting files from source directory '"
            + config.source
            + "' ("
            + err.Error()
            + ")"
        ),
        3,
        false,
    )


if config.verbose:
    print("Removing duplicate files from source list")

# TODO
# deduplicate files to avoid checking the same file twice
source_files = remove_duplicates(source_files, config.match_md5)

comparison_files = []
# comparisonFiles := make([]file, 0)
# for _, v := range config.comparison {
#     c, err := getFilesData(v, config.matchMD5)
# if err != nil {
#     fmt.Println("Failed to get files from directory '" + v + "' (" + err.Error() + ")")
# }
# comparisonFiles = append(comparisonFiles, c...)
# }

files_to_delete = find_all_duplicates(source_files, comparison_files, config)

for f in files_to_delete:
    if config.delete:
        if verbose:
            print(f"Deleting {f}")
        os.remove()  # put in try
    else:
        print(f"{f.checksum}\t{f.path}")


# func getFilesData(dir string, matchMD5 bool) ([]file, error) {
# func findAllDuplicates(sourceFiles []file, copies []file, conf config)
# func findDuplicates(f file, comparison []file, conf config) []file {
# func verifyFolders(source string, comparison []string) error {
# func removeDuplicates(fileList []file, checkMD5 bool) []file {

# func main() {
#
# }
#
# func getConfig() (config, error) {
#     source := flag.String("s", "", "Source directory.")
# separator := flag.String("p", ",", "Separator (default ',').")
# comparison := flag.String("c", "", "Directories to compare (separated by specified separator).")
# outputFile := flag.String("o", "", "File to output the list of files to delete.")
# matchFilename := flag.Bool("f", false, "Match filenames.")
# matchMD5 := flag.Bool("m", false, "Match filenames.")
# verbose := flag.Bool("v", false, "Verbose reporting.")
# delete := flag.Bool("d", false, "Delete duplicated files")
#
# flag.Parse()
# var conf config
# conf.source = *source
# conf.comparison = make([]string, 0)
# conf.outputFile = *outputFile
# conf.matchFilename = *matchFilename
# conf.matchMD5 = *matchMD5
# conf.verbose = *verbose
# conf.separator = *separator
# conf.delete = *delete
# errorMessages := ""
# if conf.source == "" {
#     errorMessages = "Source directory is required.\n"
# }
#
# if *comparison == "" {
# errorMessages += "At least one Comparison directory is required.\n"
# } else {
# conf.comparison = append(conf.comparison, strings.Split(*comparison, *separator)...)
# }
#
# if conf.matchFilename == false && conf.matchMD5 == false {
# errorMessages += "You need to specify one of -m or -f.\n"
# }
#
# if errorMessages != "" {
# fmt.Println(errorMessages)
# return conf, errors.New(errorMessages)
# }
# return conf, nil
# }
#
# // getFilesData recursively walks around a directory and records all the files it finds
#                                                                                   // optionally, it also calculates the checksum for the file
#                                                                                                                                          // returns an array of file structs and the error state
# func getFilesData(dir string, matchMD5 bool) ([]file, error) {
# files := make([]file, 0)
#
# fileData := func(path string, f os.FileInfo, err error) error {
#     switch mode := f.Mode(); {
#     case mode.IsDir():
# // do nothing
# case mode.IsRegular():
# var checksum string
# if matchMD5 {
#     checksum, err = calculateFileChecksum(path)
# if err != nil {
# return errors.New("Failed to calculate the checksum for " + path)
# }
# }
# files = append(files, file{name: f.Name(), path: path, size: f.Size(), checksum: checksum})
# }
# return nil
# }
#
# f, err := os.Stat(dir)
# if err != nil {
# return nil, errors.New("Unable to find directory or file " + dir)
# }
# if !f.Mode().IsDir() {
# return nil, errors.New(dir + " is not a directory")
# }
#
# err = filepath.Walk(dir, fileData)
#
# return files, nil
# }
#
#
# // findAllDuplicates finds all the files that are in both the sourceFiles and copies slices
#                                                                                      // returns a slice of file structs
# func findAllDuplicates(sourceFiles []file, copies []file, conf config) ([]file, error) {
# duplicates := make([]file, 0)
# for _, v := range sourceFiles {
#     duplicates = append(duplicates, findDuplicates(v, copies, conf)...)
# }
# return duplicates, nil
# }
#
# // findDuplicates finds all duplicates of a given file, according to the config settings
#                                                                                 // returns a slice of file structs
# func findDuplicates(f file, comparison []file, conf config) []file {
# duplicates := make([]file, 0)
#
# for _, v := range comparison {
#                              // Files with different size cannot have the same contents
# if f.size == v.size {
#
# // We're matching on filename
# if conf.matchFilename && f.name == v.name {
# duplicates = append(duplicates, v)
# }
#
# // We're matching on md5
# if conf.matchMD5 && f.checksum == v.checksum {
# duplicates = append(duplicates, v)
# }
# }
# }
# return duplicates
# }
#
# // verifyFolders checks that the source does not match the destination, otherwise you will be deleting the whole of the source
# func verifyFolders(source string, comparison []string) error {
#
# for _, v := range comparison {
# if v == source {
#     err := errors.New("ERROR: source and one of the directories to compare are the same (" + source + "). You really do not want to do that")
# return err
# }
# }
# return nil
# }
#
# // removeDuplicates removes duplicate entries from a list of files
# func removeDuplicates(fileList []file, checkMD5 bool) []file {
#
# // TODO - implement function
#
# // deduplicated := make([]file, len(fileList)) // Potentially over-allocating but this program is not that refined yet
#
# // // TODO change to in-place deduplication
#
# // return deduplicated
# return fileList
# }
#
# // printError prints the specified error message, the usage and then exits with the specified error code
# func exitWithError(err error, errorCode int, showFlags bool) {
# fmt.Println(err)
# if showFlags {
# fmt.Println("Options: ")
# flag.PrintDefaults()
# // printUsage()
# }
#
# os.Exit(errorCode)
#
# }
