package main

import (
	"crypto/md5"
	"errors"
	"flag"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
)

// This tool finds and exposes or deletes duplicated files in directories
// Originally based on a collection of shell scripts I wrote
// One of the targets of this tool is not to use anything out of the stdlib

// TODO
// Make the program able to find duplicates (not delete!) in a single folder
// Use streaming for checksums to reduce memory usage
// Convert the checksumming to goroutine so we can run multiple ones
// (on the USB HDD I use, it's IO bound so it probably won't buy us anything)
// Allow choosing of different checksum algorithms

// file is used for containing file information
type file struct {
	path string
	name string
	size int64
	// checksum      string
	// shortChecksum string
}

var verbose bool

// config contains settings for the program
type config struct {
	source        string
	comparison    []string
	outputFile    string
	delete        bool
	matchFilename bool
	matchMD5      bool
	verbose       bool
	separator     string
	shortMatch    bool
}

const shortMatchLength int64 = 1000

func main() {
	config, err := getConfig()

	if err != nil {
		fmt.Println("Failed to get config from command line options")
		fmt.Println(err.Error())
		os.Exit(1)
	}

	verbose = config.verbose

	comparingWithItself := false
	if verbose {
		comparison := "itself"
		if len(config.comparison) > 0 {
			comparison = strings.Join(config.comparison, ", ")
		}
		fmt.Println("Comparing", config.source, "with", comparison)
		comparingWithItself = true
	}

	if verbose {
		fmt.Println("Getting files from source directory " + config.source + ".")
	}
	sourceDirs := make([]string, 1)
	sourceDirs = append(sourceDirs, config.source)

	sourceFilesBySize, err := getFilesData(sourceDirs)
	if err != nil {
		fmt.Println("Error getting files from source directory '" + config.source)
		fmt.Println("' (" + err.Error() + ")")
		os.Exit(1)
	}
	if len(sourceFilesBySize) == 0 {
		fmt.Println("There are no files in source directory '" + config.source)
		os.Exit(1)
	}

	var filesToDelete []*file

	if comparingWithItself {
		filesToDelete, err = singleDirDuplicates(sourceFilesBySize)
	} else {
		// compFilesBySize := make(map[int64][]file)
		compFilesBySize, err := getFilesData(config.comparison)
		if err != nil {
			fmt.Println("Failed to get list of files from '" + strings.Join(config.comparison, ", "))
			fmt.Println(err.Error())
			os.Exit(1)

		}

		filesToDelete, err = multipleDirDuplicates(sourceFilesBySize, compFilesBySize)
	}
	for _, v := range filesToDelete {
		if config.delete {
			if verbose {
				fmt.Println("Deleting ", v.path)
				err := os.Remove(v.path)
				if err != nil {
					fmt.Println("Failed to remove file ", v.path, " - ")
					fmt.Println(err.Error())
				}
			}
		} else {
			fmt.Println(v.path)
		}
	}
}

// getConfig extracts and validates command line options and builds a configuration struct
// returns a config struct
func getConfig() (config, error) {
	source := flag.String("s", "", "Source directory.")
	separator := flag.String("p", ",", "Separator (default ',').")
	comparison := flag.String("c", "", "Directories to compare (separated by specified separator).")
	outputFile := flag.String("o", "", "File to output the list of files to delete.")
	matchFilename := flag.Bool("f", false, "Match filenames.")
	matchMD5 := flag.Bool("m", false, "Match checksum.")
	verbose := flag.Bool("v", false, "Verbose reporting.")
	delete := flag.Bool("d", false, "Delete duplicated files")

	flag.Parse()
	var conf config
	conf.source = *source
	conf.comparison = make([]string, 0)
	conf.outputFile = *outputFile
	conf.matchFilename = *matchFilename
	conf.matchMD5 = *matchMD5
	conf.verbose = *verbose
	conf.separator = *separator
	conf.delete = *delete

	errorMessages := ""
	if conf.source == "" {
		errorMessages = "Source directory is required.\n"
	}

	if *comparison != "" {
		conf.comparison = append(conf.comparison, strings.Split(*comparison, *separator)...)
	}

	if conf.matchFilename == false && conf.matchMD5 == false {
		errorMessages += "You need to specify one of -m or -f.\n"
	}

	if errorMessages != "" {
		fmt.Println(errorMessages)
		return conf, errors.New(errorMessages)
	}
	return conf, nil
}

func getFilesData(dirs []string) (map[int64][]file, error) {
	files := make(map[int64][]file)

	fileData := func(path string, f os.FileInfo, err error) error {
		switch mode := f.Mode(); {
		case mode.IsDir():
			// do nothing
		case mode.IsRegular():

			size := f.Size()
			files[size] = append(
				files[size],
				file{name: f.Name(), path: path, size: size})
		}
		return nil
	}

	for _, dir := range dirs {
		f, err := os.Stat(dir)
		if err != nil {
			return nil, errors.New("Unable to find directory or file " + dir)
		}
		if !f.Mode().IsDir() {
			return nil, errors.New(dir + " is not a directory")
		}

		err = filepath.Walk(dir, fileData)
	}
	return files, nil
}

// calculateFileChecksum calculates the checksum of a given file
// returns an string with the checksum and an error
// warning - reads the whole file in memory so don't use with huge files
func calculateFileChecksum(filePath string, matchLength int64) (string, error) {
	var result []byte
	file, err := os.Open(filePath)
	if err != nil {
		return "Failed to open " + filePath, err
	}
	defer file.Close()

	// TODO - stream the file
	hash := md5.New()

	if matchLength != 0 {
		if _, err := io.CopyN(hash, file, matchLength); err != nil {
			return "", err
		}
	} else {
		if _, err := io.Copy(hash, file); err != nil {
			return "", err
		}
	}

	return fmt.Sprintf("%x", hash.Sum(result)), nil
}

// inList checks if a slice of strings contains a given string
func inList(list []string, value string) bool {
	for _, v := range list {
		if v == value {
			return true
		}
	}
	return false
}

// calculateChecksum calculates the checksum of a given file path
// returns an string with the checksum and an error
// warning - reads the whole file in memory so don't use with huge files
func calculateChecksum(filePath string, matchLength int64) (string, error) {
	var result []byte
	file, err := os.Open(filePath)
	if err != nil {
		return "", err
	}
	defer file.Close()

	// TODO - stream the file
	hash := md5.New()

	if matchLength != 0 {
		if _, err := io.CopyN(hash, file, matchLength); err != nil {
			return "", err
		}
	} else {
		if _, err := io.Copy(hash, file); err != nil {
			return "", err
		}
	}

	return fmt.Sprintf("%x", hash.Sum(result)), nil
}

func singleDirDuplicates(listOfFiles map[int64][]file) ([]*file, error) {
	var filesToDelete []*file
	for _, listOfFiles := range listOfFiles {
		if len(listOfFiles) == 1 {
			// We only have a file of this length, skip
			continue
		}

		filesByShortChecksum := make(map[string][]*file)
		filesByChecksum := make(map[string][]*file)

		// var shortChecksum string
		// var longChecksum string

		for _, f := range listOfFiles {
			// Get the short Checksum of all the files of this size
			shortChecksumLength := shortMatchLength
			if f.size < shortMatchLength {
				// The file is longer than the shortMatchLength
				shortChecksumLength = f.size
			}
			shortChecksum, err := calculateChecksum(f.path, shortChecksumLength)
			if err != nil {

			}
			filesByShortChecksum[shortChecksum] = append(filesByShortChecksum[shortChecksum], &f)

			for _, fds := range filesByShortChecksum {
				if len(fds) == 1 {
					continue
				}

				for _, fd := range fds {
					checksum, err := calculateChecksum(fd.path, 0)
					if err != nil {
						return nil, errors.New("Failed to get checksum for " + fd.path)
					}
					filesByChecksum[checksum] = append(filesByChecksum[checksum], &f)
				}
			}

			for _, fds := range filesByChecksum {
				if len(fds) == 1 {
					continue
				}

				for i, f := range fds {
					if i == 0 {
						// We skip the first one
						continue
					} else {
						filesToDelete = append(filesToDelete, f)
					}
				}
			}
		}
	}
	return filesToDelete, nil
}

func multipleDirDuplicates(source map[int64][]file, comparison map[int64][]file) ([]*file, error) {

	var filesToDelete []*file

	// Get the short checksum of source files

	sourceFilesByShortChecksum := make(map[string][]*file)
	for size, sourceFiles := range source {
		if _, found := comparison[size]; !found {
			// There are no files of the same length in the comparison dirs, skip
			continue
		}

		for _, sf := range sourceFiles {
			shortChecksumLength := shortMatchLength
			if sf.size < shortMatchLength {
				// The file is longer than the shortMatchLength
				shortChecksumLength = sf.size
			}
			shortChecksum, err := calculateChecksum(sf.path, shortChecksumLength)
			if err != nil {
				return nil, errors.New("Unable to calculate short checksum of file " + sf.path)
			}
			sourceFilesByShortChecksum[shortChecksum] = append(sourceFilesByShortChecksum[shortChecksum], &sf)
		}
	}

	// Get the short checksum of comparison files
	filesByShortChecksum := make(map[string][]*file)

	for size, compFiles := range comparison {
		if _, found := source[size]; !found {
			// There are no files of the same length in the comparison dirs, skip
			continue
		}

		for _, f := range compFiles {
			shortChecksumLength := shortMatchLength
			if f.size < shortMatchLength {
				// The file is longer than the shortMatchLength
				shortChecksumLength = f.size
			}
			shortChecksum, err := calculateChecksum(f.path, shortChecksumLength)
			if err != nil {
				return nil, errors.New("Unable to calculate short checksum of file " + f.path)
			}
			// Only append to the list if we have a matching short checksum in the source
			if len(sourceFilesByShortChecksum[shortChecksum]) > 0 {
				filesByShortChecksum[shortChecksum] = append(filesByShortChecksum[shortChecksum], &f)
			}

		}
	}

	// Calculate full checksums

	// At this point filesByShortChecksum should be way smaller than sourceFilesByShortChecksum
	//  so we use it to loop through and calculate full checksums

	filesByChecksum := make(map[string][]*file)

	for cs, fs := range filesByShortChecksum {
		// This check should be redundant but, just in case
		if _, found := sourceFilesByShortChecksum[cs]; !found {
			// There are no files of the same short checksum in the source dir, skip
			continue
		}
		for _, f := range fs {
			checksum, err := calculateChecksum(f.path, 0)
			if err != nil {
				return nil, errors.New("Unable to calculate checksum for " + f.path)
			}
			filesByChecksum[checksum] = append(filesByChecksum[checksum], f)
		}

	}

	sourceFilesByChecksum := make(map[string][]*file)

	// Get the checksums of the source dir
	for cs, fs := range sourceFilesByShortChecksum {
		if _, found := filesByChecksum[cs]; !found {
			// There are no files of the same checksum in the copy dir, skip
			continue
		}

		for _, f := range fs {
			checksum, err := calculateChecksum(f.path, 0)
			if err != nil {
				return nil, errors.New("Unable to calculate checksum for " + f.path)
			}
			sourceFilesByChecksum[checksum] = append(sourceFilesByChecksum[checksum], f)
		}
	}

	// Finally, get a listof the files to delete
	// It probably can be done in the previous step, while calculating checksums
	// but that would prevent us from refactoring the 4 previous sections into
	// function calls
	for cs, fs := range filesByChecksum {
		if _, found := sourceFilesByChecksum[cs]; !found {
			// There are no files of the same checksum in the copy dir, skip
			continue
		}

		for _, f := range fs {
			// All matched files will be deleted
			filesToDelete = append(filesToDelete, f)
		}
	}
	return filesToDelete, nil
}
