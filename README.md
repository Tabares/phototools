Photosort

Photosort is a command line tool to organise pictures and videos (potentially, any file that contains data supported by Hachier) into directories by date via their EXIF information.

```
Usage: photosort.py [options]

Options:
  -h, --help            show this help message and exit
  -s SOURCE, --source=SOURCE
                        Directory to get pictures from.
  -t TARGET, --target=TARGET
                        Target directory to move pictures to.
  -m, --merge           Merge pictures into directories (do not create folders
                        when a directory starting with the same date exists).
  -o, --overwrite       Overwrite existing files with the same name.
  -d, --die             Die on error.
  -v, --verbose         Enable verbose mode.
 ```

Files for which the date cannot be determined will not be moved.