#!/usr/bin/env python3

import glob
import os
import sys

import exifread
import argparse

from hachoir.parser import createParser
from hachoir.metadata import extractMetadata
from hachoir.core import config as HachoirConfig

HachoirConfig.quiet = True

"""
Photosort will sort your images, videos and any other file it can understand into date-based folders in ISO-8601 format via EXIF information.
"""


class Config:
    def __init__(self):
        source = ""
        target = ""
        merge = False
        overwrite = False
        debug = False
        die_on_error = False


class ImageFile:
    def __init__(self):
        path = ""
        full_path = ""
        date = None


def parse_opts():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-s", "--source", dest="source", help="Directory to get pictures from."
    )
    parser.add_argument(
        "-t", "--target", dest="target", help="Target directory to move pictures to."
    )
    parser.add_argument(
        "-m",
        "--merge",
        dest="merge",
        action="store_true",
        default=False,
        help="Merge pictures into directories (do not create folders when a directory starting with the same date exists).",
    )
    parser.add_argument(
        "-o",
        "--overwrite",
        dest="overwrite",
        action="store_true",
        default=False,
        help="Overwrite existing files with the same name.",
    )
    parser.add_argument(
        "-d",
        "--die",
        dest="die",
        action="store_true",
        default=False,
        help="Die on error.",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        dest="verbose",
        action="store_true",
        default=False,
        help="Enable verbose mode.",
    )

    return parser.parse_args()


def get_config(options):

    config = Config()
    errors = []

    if not options.source:
        errors.append("Source directory is required")
    else:
        config.source = options.source

    if not options.target:
        errors.append("Target directory is required")
    else:
        config.target = options.target

    if options.source == options.target and errors == []:
        errors.append("Source and destinatin can not be the same")

    if errors:
        exitWithStatus("\n".join(errors), 1)

    config.merge = options.merge
    config.overwrite = options.overwrite
    config.die_on_error = options.die
    config.verbose = options.verbose

    return config


def validate_paths(options):
    errors = []
    if not os.path.isdir(options.source):
        errors.append(
            f"Source directory '{options.source}' does not exist or is not a directory."
        )

    if not os.path.isdir(options.target):
        errors.append(
            f"Target directory '{options.target}' does not exist or is not a directory."
        )

    if errors:
        exitWithStatus("\n".join(errors), 1)


def find_files(dir):
    image_files = []

    for root, _, files in os.walk(dir):
        for file in files:
            # _, file_extension = os.path.splitext(file)

            img = ImageFile()
            img.path = file
            img.full_path = os.path.join(root, file)
            if config.verbose:
                print("Processing " + img.full_path)
            img.date = get_creation_date(img.full_path)
            if img.date:
                image_files.append(img)

    return image_files


def get_creation_date(path):
    # read image exif
    parser = createParser(path)
    if parser:
        try:
            metadata = extractMetadata(parser)
        except:
            if config.die_on_error:
                exitWithStatus("Error getting metadata from " + path + ". Exiting", 1)

    try:
        date = metadata.getValues("creation_date")[0].strftime("%Y-%m-%d")
        return date
    except:
        # Hachoir failed, let's use exifread instead

        if config.verbose:
            print("Hachoir failed, using exifread instead")
        try:
            f = open(path, "rb")
            tags = exifread.process_file(f, details=False, stop_tag="Image DateTime")
            f.close()
            date = tags.get("Image DateTime").printable[0:10]
        except:
            if config.die_on_error:
                exitWithStatus("Error extracting data from " + path + ". Exiting", 1)
            else:
                if config.verbose:
                    print(
                        "Error extracting data from "
                        + path
                        + ", file will not be moved."
                    )
                return None
        return date[0:4] + "-" + date[5:7] + "-" + date[8:10]


def get_dir_list(images):
    dirs = []

    for img in images:
        if not img.date in dirs:
            dirs.append(img.date)

    return dirs


def move_files(images):
    dirs = get_dir_list(images)

    for dir in dirs:

        if config.verbose:
            print("Processing images for " + dir)

        target_dir = None
        if config.merge:
            day_dirs = glob.glob(os.path.join(config.target, dir + "*"))
            if day_dirs:
                for d in day_dirs:
                    if os.path.isdir(d):
                        target_dir = d
                        break

                if config.verbose:
                    print("Merging " + dir + " to " + target_dir)

        if not target_dir:
            target_dir = os.path.join(config.target, dir)

            if config.verbose:
                print("Creating target dir " + target_dir)

            if not os.path.isdir(target_dir):
                try:
                    os.mkdir(target_dir)
                except:
                    if config.die_on_error:
                        exitWithStatus(
                            "Failed to create dir " + target_dir + ". Exiting.", 1
                        )
                    print("Failed to create dir " + target_dir)

        for image in images:
            target_file = os.path.join(target_dir, image.path)
            if image.date == dir:
                target_file_exists = os.path.isfile(target_file)
                if target_file_exists:
                    if not config.overwrite:
                        if config.die_on_error:
                            exitWithStatus(
                                "Target file exists " + target_file + ". Exiting", 1
                            )
                        else:
                            if config.verbose:
                                print("Not overwriting " + target_file)
                                break
                    else:
                        if config.verbose:
                            print("File exists, overwriting")
                try:
                    os.rename(image.full_path, target_file)
                    if config.verbose:
                        print("Moving " + image.full_path + " to " + target_file)

                except:
                    if config.die_on_error:
                        exitWithStatus(
                            "Error moving "
                            + image.full_path
                            + " to "
                            + target_file
                            + ". Exiting",
                            1,
                        )
                    else:
                        print("Error moving " + image.full_path + " to " + target_file)


def exitWithStatus(message, status):
    print(message)
    sys.exit(status)


options = parse_opts()
config = get_config(options)
validate_paths(options)
images = find_files(config.source)

move_files(images)
